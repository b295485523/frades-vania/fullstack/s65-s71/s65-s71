 const productsData = [
            {
                id: "64ae06f6b9c0fe2d44a41bc8",
                name: "Cowrie Shell Chokerl",
                description: "A trendy cowrie shell choker necklace that adds a bohemian touch to any look. Perfect for creating a beach-inspired style.",
                price: 99,
                onOffer: true
            },
            {
                id: "4ae07c7b9c0fe2d44a41bca",
                name: "Coconut Shell Necklace",
                description: "Get ready for a tropical vibe with this layered necklace featuring coconut shells.",
                price: 99,
                onOffer: true
            },
            {
                id: "64ae089bb9c0fe2d44a41bcc",
                name: "JPuka Shell Necklace",
                description: "Crafted with sterling silver, this necklace features a delicate chain and a stunning layer of Puka shells fresh from the sea.",
                price: 199,
                onOffer: true
            },
            {
                id: "64ae15b20182e811a4173936",
                name: "Puka Shell Bracelet",
                description: "This bracelet features a delicate chain and a stunning layer of Puka shells fresh from the sea.",
                price: 129,
                onOffer: true
            }
        ]

        export default productsData;
