import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

const centerStyle = {
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  minHeight: '100vh',
};

export default function ProductView() {


  const { productId } = useParams();
  const { user } = useContext(UserContext);
  const navigate = useNavigate();

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0);
  const [quantity, setQuantity] = useState(1);

  const [orderSuccess, setOrderSuccess] = useState(false);

  useEffect(() => {
    console.log(productId);

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
      });
  }, [productId]);

  const order = async () => {
    try {
      const response = await 
      fetch(`${process.env.REACT_APP_API_URL}/orders`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
        body: JSON.stringify({
          productId,
          quantity,
        }),
      });

      if (response.ok) {
        setOrderSuccess(true);
        Swal.fire('Order Success', 'Your order has been placed successfully!', 'success');
      } else {
        console.error('Order failed.');
        Swal.fire('Order Failed', 'Failed to place your order.', 'error');
      }
    } catch (error) {
      console.error('Error making order:', error);
      Swal.fire('Order Error', 'An error occurred while placing your order.', 'error');
    }
  };

const addToCart = () => {
    const itemToAdd = {
      productId,
      productName: name,
      quantity,
      price,
      subtotal: price * quantity,
    };

    // Make an API request to add the item to the cart
    fetch(`${process.env.REACT_APP_API_URL}/users/cart/add`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify(itemToAdd)
    })
    .then(res => res.json())
    .then(data => {
      if (data) {
        Swal.fire({
          title: "Successfully added to cart",
          icon: "success",
          text: "The product has been added to your cart."
        });
      } else {
        Swal.fire({
          title: "Something went wrong",
          icon: "error",
          text: "Please try again."
        });
      }
    })
    .catch(error => {
      console.error(error);
      Swal.fire({
        title: "Error",
        icon: "error",
        text: "An error occurred while processing your request. Please try again later."
      });
    });
  };

return (
    <div style={centerStyle}>
      <Container>
        <Row>
          <Col lg={{ span: 6, offset: 3 }}>
            <Card>
              <Card.Body className="text-center">
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {price}</Card.Text>
                <Card.Subtitle>Quantity: </Card.Subtitle>
                  <Form.Control
                    type="number"
                    min="1"
                    value={quantity}
                    onChange={(e) => setQuantity(parseInt(e.target.value))}
                  />                
                {user.id !== null ? (
                  <>
                  <Button variant="primary" onClick={order}>
                    Order
                  </Button>

                  <Button variant="warning" onClick={addToCart}>
                    Add to Cart
                  </Button>
                  </>
                ) : (
                  <Button as={Link} to="/login" variant="danger">
                    Log in to Order Item
                  </Button>
                )}
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </div>
  );
}