import { useState, useEffect, useContext } from 'react';
import { Row, Col, Container } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import ResetPassword from '../components/ResetPassword';
import UpdateProfile from '../components/UpdateProfile';
import MyOrders from '../components/MyOrders';

export default function Profile() {
  const { user } = useContext(UserContext);

  const [details, setDetails] = useState({});

  useEffect(() => {

    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (typeof data._id !== 'undefined') {
          setDetails(data);
        }
      });
  }, []);

  return (
    <>
      {user.id === null ? (
        <Navigate to="/products" />
      ) : (
        <Container className="my-3 p-5 bg-primary text-white " style={{ border: '1px solid #ccc', borderRadius: '5px' }}>
          <h1 className="my-5">Profile</h1>
          <h2 className="mt-3">{`${details.firstName} ${details.lastName}`}</h2>
          <hr />
          <h4>Contacts</h4>
          <ul>
            <li>Email: {details.email}</li>
            <li>Mobile No: {details.mobileNo}</li>
          </ul>
          <hr />
        </Container>
      )}

       {user.id !== null && (
        <Row className="mt-4">
          <Col className="d-flex justify-content-center">
            <Container className="p-3 bg-light text-center" style={{ maxWidth: '500px', border: '1px solid #ccc', borderRadius: '5px' }}>
              <MyOrders />
            </Container>
          </Col>
        </Row>
      )}


      {user.id !== null && (
        <Row className="mt-4">
          <Col className="d-flex justify-content-center">
            <Container className="p-3 bg-light text-center" style={{ maxWidth: '500px', border: '1px solid #ccc', borderRadius: '5px' }}>
              <UpdateProfile />
            </Container>
          </Col>
        </Row>
      )}

      {user.id !== null && (
        <Row className="mt-4">
          <Col className="d-flex justify-content-center">
            <Container className="p-3 bg-light text-center" style={{ maxWidth: '500px', border: '1px solid #ccc', borderRadius: '5px' }}>
              <ResetPassword />
            </Container>
          </Col>
        </Row>
      )}

      
    </>
  );
}