import React from 'react';
import { Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const Error = () => {
  return (
    <Row>
      <Col className="p-5 text-center">
        <h3>ERROR</h3>
        <h1>Page Not Found</h1>
        <p>Go back to the <Link to="/">homepage</Link>.</p>
      </Col>
    </Row>
  );
};

export default Error;