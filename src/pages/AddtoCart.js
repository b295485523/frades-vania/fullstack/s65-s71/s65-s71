import { useEffect, useState, useContext } from 'react';
import Swal from 'sweetalert2'
import UserContext from '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';

const CartPage = () => {

  const { user } = useContext(UserContext);
  const [cartItems, setCartItems] = useState([]);
  const [totalPrice, setTotalPrice] = useState(0);

  const [isPurchaseSuccess, setIsPurchaseSuccess] = useState(false);
  const navigate = useNavigate();

  useEffect(() => {
    // Fetch cart items from the backend API
    fetch(`${process.env.REACT_APP_API_URL}/users/cart/items`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      if (data && data.cartItems) {
        setCartItems(data.cartItems);
        const total = data.cartItems.reduce((acc, item) => acc + item.subtotal, 0);
        setTotalPrice(total);
      }
    })
    .catch(error => {
      console.error(error);
    });
  }, []);

  useEffect(() => {
    // Update total price whenever cart items change
    const total = cartItems.reduce((acc, item) => acc + item.subtotal, 0);
    setTotalPrice(total);
  }, [cartItems]);



useEffect(() => {
    if (isPurchaseSuccess) {
      navigate('/'); 
    }
  }, [isPurchaseSuccess, navigate]);


  return (

    (user.isAdmin) ?
      <Navigate to="/products" />
      :
    <div className="cart-container">
          <h2 className="cart-head">My Cart</h2>
          <table className="table">
            <thead>
              <tr>
                <th>Product Name</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Subtotal</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {cartItems.map(item => (
                <tr key={item.productId}>
                  <td>{item.productName}</td>
                  <td>
                    <input
                      type="number"
                      className="form-control"
                      min="1"
                      value={item.quantity}
                      
                    />
                    </td>
                  <td>₱{item.price}</td>
                  <td>₱{item.subtotal}</td>
                  <td>
                    <button className="btn btn-danger">
                      Remove
                    </button>
                  </td>
                  
                </tr>
              ))}
            </tbody>
            <tfoot>
              <tr>
                <td colSpan="3">
                    <button className="btn btn-primary">
                      Checkout
                    </button>
                </td>
                <td colSpan="2"> Total Price: ₱{totalPrice}</td>
              </tr>
            </tfoot>
          </table>
        </div>
  );
};

export default CartPage;