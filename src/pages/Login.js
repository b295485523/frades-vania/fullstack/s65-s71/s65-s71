import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {
  const { user, setUser } = useContext(UserContext);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(false);
  const [showPassword, setShowPassword] = useState(false);

function loginUser(e) {
  e.preventDefault();

  fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      email: email,
      password: password,
    }),
  })
    .then((res) => res.json())
    .then((data) => {
      console.log(data);

      if (data.access) {
        localStorage.setItem('token', data.access);
        retrieveUserDetails(data.access);

        Swal.fire({
          title: 'Login Successful',
          icon: 'success',
          text: 'Hey there, Welcome!',
          showConfirmButton: false,
          timer: 1500,
        });
      } else {
        Swal.fire({
          title: 'Authentication Failed',
          icon: 'error',
          text: 'Check your login details and try again.',
          showConfirmButton: false,
          timer: 1500,
        });
      }
    });

  setEmail('');
  setPassword('');
}


  const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        })
      })
  }

  useEffect(() => {
    setIsActive(email.trim() !== '' && password.trim() !== '');
  }, [email, password]);

  return( (user.id !== null) ? 
    <Navigate to="/products" />
    : (
    <Container
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100vh',
      }}
    >
      <div style={{ width: '400px', border: '1px solid #ccc', borderRadius: '5px', boxShadow: '0 2px 4px rgba(0, 0, 0, 0.1)', padding: '20px' }}>
        <Form style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }} onSubmit={(e) => loginUser(e)}>
          <h1 style={{ textAlign: 'center', marginBottom: '30px' }}>Login</h1>

          <Form.Group style={{ marginBottom: '20px' }} controlId="Email">
            <Form.Label>Email Address</Form.Label>
            <Form.Control type="email" placeholder="Enter email" required value={email} onChange={(e) => setEmail(e.target.value)} />
          </Form.Group>

          {/*additional show password*/}
          <Form.Group style={{ marginBottom: '20px' }} controlId="Password">
            <Form.Label>Password</Form.Label>
            <Form.Control type={showPassword ? 'text' : 'password'} placeholder="Password" required value={password} onChange={(e) => setPassword(e.target.value)} />
          </Form.Group>

          <div style={{ display: 'flex', alignItems: 'center', marginBottom: '10px' }}>
            <input type="checkbox" checked={showPassword} onChange={() => setShowPassword(!showPassword)} style={{ marginRight: '5px' }} />
            <span>Show Password</span>
          </div>

          <div style={{ display: 'flex', justifyContent: 'center' }}>
            <Button variant="primary" type="submit" id="submitBtn" disabled={!isActive}>
              Submit
            </Button>
          </div>
        </Form>
      </div>
    </Container>
  )
  )
}
