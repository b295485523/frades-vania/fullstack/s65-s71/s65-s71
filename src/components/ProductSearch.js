import React, { useState } from 'react';
import ProductCard from './ProductCard';
import { Container } from 'react-bootstrap';

const ProductSearch = () => {
  const [searchQuery, setSearchQuery] = useState('');
  const [searchResults, setSearchResults] = useState([]);

  const handleSearch = async () => {
  try {
    const response = await fetch(`${process.env.REACT_APP_API_URL}/products/`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ productName: searchQuery }),
    });
    const data = await response.json();
    setSearchResults(data);
  } catch (error) {
    console.error('Error searching for products:', error);
  }
};


  return (
    <Container fluid className="border mt-5 p-4">
      <h2>Product Search</h2>
      <div className="form-group">
        <label htmlFor="productName">Product Name:</label>
        <input
          type="text"
          id="productName"
          className="form-control"
          value={searchQuery}
          onChange={event => setSearchQuery(event.target.value)}
        />
      </div>
      <button className="btn btn-primary my-4" onClick={handleSearch}>
        Search
      </button>
      <h3>Search Results:</h3>
      <ul>
        {searchResults.map(product => (
          <ProductCard product={product} key={product._id} />
        ))}
      </ul>

    </Container>
  )
};

export default ProductSearch;
