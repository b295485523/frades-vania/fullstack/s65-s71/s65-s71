import { useState, useEffect, useContext } from 'react';
import { Container, Table } from 'react-bootstrap';
import UserContext from '../UserContext';

export default function MyOrders() {
  const { user } = useContext(UserContext);
  const [orders, setOrders] = useState([]);

  useEffect(() => {

    fetch(`${process.env.REACT_APP_API_URL}/order?userId=${user.id}`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setOrders(data);
      })
      .catch((error) => {
        console.error('Error fetching order history:', error);
      });
  }, [user.id]);

  return (
    <Container className="my-3 p-5 bg-light" style={{ border: '1px solid #ccc', borderRadius: '5px' }}>
      <h2>Order History</h2>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Order ID</th>
            <th>Product Name</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Total Price</th>
          </tr>
        </thead>
        <tbody>
          {orders.map((order) => (
            <tr key={order._id}>
              <td>{order._id}</td>
              <td>{order.product.name}</td>
              <td>${order.product.price.toFixed(2)}</td>
              <td>{order.quantity}</td>
              <td>${(order.product.price * order.quantity).toFixed(2)}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </Container>
  );
}
