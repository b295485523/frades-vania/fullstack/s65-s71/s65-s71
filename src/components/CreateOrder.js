import React from 'react';

export default function CreateOrder({ cartItems }) {
  return (
    <div>
      <h2>Cart</h2>
      {cartItems.map((item) => (
        <div key={item._id}>
          <h4>{item.name}</h4>
          <p>Price: ${item.price.toFixed(2)}</p>
        </div>
      ))}
    </div>
  );
}
