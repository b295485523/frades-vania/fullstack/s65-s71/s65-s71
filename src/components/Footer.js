import React from 'react';

export default function Footer() {

  return (
    <footer style={{ backgroundColor: 'rgba(51, 51, 51, 0.75)', color: '#fff', padding: '10px', textAlign: 'center' }}>
      <div className="container">
        <div className="row">
          <div className="col-md-4">
            <h5>Contact Us</h5>
            <p>Email: contact@example.com</p>
            <p>Phone: +1 123-456-7890</p>
          </div>
          <div className="col-md-4">
            <h5>Follow Us</h5>
            <ul style={{ listStyleType: 'none', padding: 0 }}>
              <li><a href="#" style={{ color: '#fff', display: 'block', marginBottom: '10px' }}>Facebook</a></li>
              <li><a href="#" style={{ color: '#fff', display: 'block', marginBottom: '10px' }}>Youtube</a></li>
              <li><a href="#" style={{ color: '#fff', display: 'block', marginBottom: '10px' }}>Instagram</a></li>
            </ul>
          </div>
          <div className="col-md-4">
            <h5>Company Info</h5>
            <p>About Us</p>
            <p>Privacy Policy</p>
            <p>Terms of Service</p>
          </div>
        </div>
      </div>
    </footer>
  );
};