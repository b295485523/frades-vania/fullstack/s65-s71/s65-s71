import React, { useState } from 'react';

export default function UpdateProfile(){
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [message, setMessage] = useState('');
  const [ newProfile ] = useState('');
  
  const handleUpdateProfile = async () => {
    setMessage('');

    const profileData = {
      firstName,
      lastName,
      mobileNo
    };

    try {
      const token = localStorage.getItem('token'); 
      const response = await fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify(profileData)
      });

      if (response.ok) {
        setMessage('Profile updated successfully');
        setFirstName('');
        setLastName('');
        setMobileNo('');
      } else {
        const errorData = await response.json();
        setMessage(errorData.message);
      }
    } catch (error) {
      setMessage('An error occurred. Please try again.');
      console.error(error);
    }
  };

  return (
    <div className="container mt-4">
      <h2>Update Profile</h2>
      {message && <div className="alert alert-danger">{message}</div>}
      <div className="form-group">
        <label htmlFor="firstName">First Name</label>
        <input
          type="text"
          className="form-control"
          id="firstName"
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
        />
      </div>
      <div className="form-group">
        <label htmlFor="lastName">Last Name</label>
        <input
          type="text"
          className="form-control"
          id="lastName"
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
        />
      </div>
      <div className="form-group">
        <label htmlFor="mobileNo">Mobile Number</label>
        <input
          type="tel"
          className="form-control"
          id="mobileNo"
          value={mobileNo}
          onChange={(e) => setMobileNo(e.target.value)}
        />
      </div>
      <button type="submit" className="btn btn-primary" onClick={handleUpdateProfile}>
        Update Profile
      </button>
    </div>
  );
};