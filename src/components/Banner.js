import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner() {


  const bannerStyle = {
    backgroundImage: `linear-gradient(rgba(255, 255, 255, 0.5), rgba(255, 255, 255, 0.2)), url(${process.env.PUBLIC_URL}/cover.jpg)`,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    height: '460px',
  };


  return (
    <Row>
      <Col className="p-5 text-center" style={bannerStyle}>
        <div className="d-flex justify-content-center align-items-center mb-4">

        <Link to="/products">
        <div className="image-container">
          <img
            src={process.env.PUBLIC_URL + '/ana.png'}
            alt="AVA"
            style={{
              maxWidth: '150px',
              maxHeight: '150px',
              borderRadius: '50%',
              marginRight: '30px',
              boxShadow: '0 4px 8px rgba(0, 0, 0, 0.25)',
              transition: 'transform 0.2s ease',
            }}
            className="hover-effect"
          />
        </div>
        </Link>

        <Link to="/products">
        <div className="image-container">
          <img
            src={process.env.PUBLIC_URL + '/aya.png'}
            alt="AYA"
            style={{
              maxWidth: '150px',
              maxHeight: '150px',
              borderRadius: '50%',
              margin: '30px',
              boxShadow: '0 4px 8px rgba(0, 0, 0, 0.25)',
              transition: 'transform 0.2s ease',
            }}
            className="hover-effect"
          />
        </div>
        </Link>

        <Link to="/products">
        <div className="image-container">
          <img
            src={process.env.PUBLIC_URL + '/ava.png'}
            alt="ANA"
            style={{
              maxWidth: '150px',
              maxHeight: '150px',
              borderRadius: '50%',
              marginLeft: '30px',
              boxShadow: '0 4px 8px rgba(0, 0, 0, 0.25)',
              transition: 'transform 0.2s ease',
            }}
            className="hover-effect"
          />
        </div>
        </Link>

        </div>

        <h1>SEACHEDELIC® ACCESSORIES</h1>
        <p>Get ready for a tropical vibe.</p>
        <Link to="/products">
          <Button variant="primary">Buy Now!</Button>
        </Link>
      </Col>
    </Row>
  );
}
