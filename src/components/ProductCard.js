import { useState, useEffect } from 'react';
import {Row, Col, Card, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProductCard({product}) {

	const {_id, name, description, price} = product;

	return(

		<Row>
			<Col xs={12} md={6}>
				<Card id="productComponent1 p-3">
					  <Card.Body>
						    <Card.Title>{name}</Card.Title>
						    <Card.Subtitle>Description:</Card.Subtitle>
						    <Card.Text>{description}</Card.Text>
						    <Card.Subtitle>Price:</Card.Subtitle>
						    <Card.Text>{price}</Card.Text>
						    <Button as={Link} to={`/products/${_id}`} variant="primary">View Details</Button>
					  </Card.Body>
				</Card>
			</Col>
		</Row>
	)
};