import { useEffect, useState } from 'react';
import ProductCard from './ProductCard';
import ProductSearch from './ProductSearch';
import SearchByPrice from './SearchByPrice';

export default function UserView({ productsData, fetchData }) {
  const [products, setProducts] = useState([]);
  const [cartItems, setCartItems] = useState([]);

  useEffect(() => {

    console.log('Products Data:', productsData);
    const activeProducts = productsData.filter((product) => product.isActive === true);
    setProducts(activeProducts);
  }, [productsData]);

  const addToCart = (product) => {
    setCartItems((prevCartItems) => [...prevCartItems, product]);
  };

  return (
    <>
      {products.map((product) => (
        <ProductCard
          key={product._id}
          product={product}
          addToCart={addToCart}
          fetchData={fetchData}
        />
      ))}
      <ProductSearch />
      <SearchByPrice />
    </>
  );
}