import React, { useEffect, useState } from 'react';
import { Container, Row, Col, Table, Button } from 'react-bootstrap';
import EditProduct from './EditProduct';
import ArchiveProduct from './ArchiveProduct';
import ProductSearch from './ProductSearch';
import AddProduct from './AddProduct';


export default function AdminView({ productsData, fetchData }) {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    setProducts(productsData);
  }, [productsData]);

  return (
    <Container>
      <h1 className="text-center my-4">Admin Dashboard</h1>
      <Row>
        <Col>
          <Table striped bordered hover responsive>
            <thead>
              {/* ... Table header ... */}
            </thead>
            <tbody>
              {products.map((product) => (
                <tr key={product._id}>
                  <td>{product._id}</td>
                  <td>{product.name}</td>
                  <td>{product.description}</td>
                  <td>{product.price}</td>
                  <td className={product.isActive ? 'text-success' : 'text-danger'}>
                    {product.isActive ? 'Available' : 'Archived'}
                  </td>
                  <td>
                    <EditProduct product={product._id} fetchData={fetchData} />
                  </td>
                  <td>
                    <ArchiveProduct productId={product._id} isActive={product.isActive} fetchData={fetchData} />
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
              <AddProduct />
              <ProductSearch />
        </Col>
      </Row>
    </Container>
  );
}

