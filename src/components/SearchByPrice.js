import React, { useState } from 'react';
import { Container } from 'react-bootstrap';

export default function SearchByPrice({ token }) {

  const [minPrice, setMinPrice] = useState('');
  const [maxPrice, setMaxPrice] = useState('');
  const [products, setProducts] = useState([]);


  const handleSearchProducts = () => {

    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify({ 
        minPrice: minPrice, 
        maxPrice: maxPrice
      })
    };

    fetch(`${process.env.REACT_APP_API_URL}/products`, requestOptions)
      .then(response => response.json())
      .then(data => {
        setProducts(data.products);
      })
      .catch(error => {
        console.error('Error searching for products:', error);
      });
  };

  return (
    <Container fluid className="border mt-4 p-4">
      <h2>Search Products by Price Range</h2>
      <div className="form-group">
        <label htmlFor="minPrice">Min Price</label>
        <input
          className="form-control"
          type="number"
          placeholder="Min Price"
          value={minPrice}
          onChange={(e) => setMinPrice(e.target.value)}
        />
      </div>
      <div className="form-group">
        <label htmlFor="maxPrice">Max Price</label>
        <input
          className="form-control"
          type="number"
          placeholder="Max Price"
          value={maxPrice}
          onChange={(e) => setMaxPrice(e.target.value)}
        />
      </div>
      <button type="submit" className="btn btn-primary my-4" onClick={handleSearchProducts}>
        Search
      </button>
      <h3>Search Results</h3>
      {products.length > 0 ? (
        <ul>
          {products.map((product) => (
            <li key={product._id}>{product.name}</li>
          ))}
        </ul>
      ) : (
        <p>No Result Found</p>
      )}
    </Container>
  );
};
