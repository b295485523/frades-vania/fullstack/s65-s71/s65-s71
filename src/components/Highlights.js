import {Row, Col, Card, } from 'react-bootstrap';


export default function Highlights() {

	return (

		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				      <Card.Body>
				        <Card.Title>Purchase Online</Card.Title>
				        <Card.Text>
				          Shop our exquisite collection online and embrace elegance at your fingertips.
				        </Card.Text>
				      </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				      <Card.Body>
				        <Card.Title>Get ready for a tropical vibe.</Card.Title>
				        <Card.Text>
				          Unleash your inner island spirit with our vibrant and tropical accessories.
				        </Card.Text>
				      </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				      <Card.Body>
				        <Card.Title>Shells fresh from the sea!</Card.Title>
				        <Card.Text>
				          Discover the natural beauty of shells handpicked from the ocean's depths.
				        </Card.Text>
				      </Card.Body>
				</Card>
			</Col>
		
		</Row>

	)

};